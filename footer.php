
		<!-- popup -->
		<div class="popUp" id="thanks">
			<div class="popUp_wrap">
				<div class="popUp_content feedBack">
					<img src="/f/close.svg" class="closePopUp" alt="">
					<div class="popUp_wrapFrom">
						<h2>Спасибо за заявку</h2>
						<p>Мы вам перезвоним</p>
						<div class="btnWrap">
							<div class="borderBTN closePopUp_link"><span>Хорошо</span></div>
						</div>
					</div>
				</div>
				<div class="popUp_shade"></div>
			</div>
		</div>

		<!-- popup -->
		<div class="popUp" id="terms-of-use">
			<div class="popUp_wrap">
				<div class="popUp_content feedBack">
					<img src="/f/close.svg" class="closePopUp" alt="">
					<div class="popUp_wrapFrom">
						<h2>Соглашение на обработку персональных данных</h1>						
						<p>Предоставляя свои персональные данные ИП Тихомирова посредством сайта <b>3owls.ru</b>, Пользователь даёт согласие на обработку, хранение и использование своих персональных данных на основании ФЗ № 152-ФЗ «О персональных данных» от 27.07.2006 г. в следующих целях:
						</p>
						<ul>
						<li>Осуществление клиентской поддержки</li>
						<li>Получения Пользователем информации о маркетинговых событиях</li>
						<li>Проведения аудита и прочих внутренних исследований с целью повышения качества предоставляемых услуг.</li>
						</ul>
						<p>
						Под персональными данными подразумевается любая информация личного характера, позволяющая установить личность Пользователя/Покупателя такая как:
						</p>
						<ul>
						<li>Фамилия, Имя, Отчество</li>
						<li>Контактный телефон</li>
						<li>Адрес электронной почты</li>
						</ul>
						<p>
						Персональные данные Пользователей хранятся исключительно на электронных носителях и обрабатываются с использованием автоматизированных систем, за исключением случаев, когда неавтоматизированная обработка персональных данных необходима в связи с исполнением требований законодательства.
						</p>
						<p>
						Компания обязуется не передавать полученные персональные данные третьим лицам, за исключением следующих случаев:
						</p>
						<ul>
						<li>По запросам уполномоченных органов государственной власти РФ только по основаниям и в порядке, установленным законодательством РФ</li>
						<li>Стратегическим партнерам, которые работают с Компанией для предоставления продуктов и услуг, или тем из них, которые помогают Компании реализовывать продукты и услуги потребителям. Мы предоставляем третьим лицам минимальный объем персональных данных, необходимый только для оказания требуемой услуги или проведения необходимой транзакции.</li>
						</ul>
						<p>
						Компания оставляет за собой право вносить изменения в одностороннем порядке в настоящие правила, при условии, что изменения не противоречат действующему законодательству РФ. Изменения условий настоящих правил вступают в силу после их публикации на Сайте.
						</p>
						<p>Дата обновления соглашения: 19 октября 2018г.</p>
						 
						<div class="btnWrap">
							<div class="borderBTN closePopUp_link"><span>Хорошо</span></div>
						</div>
					</div>
				</div>
				<div class="popUp_shade"></div>
			</div>
		</div>

		<!-- popup -->
		<div class="popUp" id="cookie">
			<div class="popUp_wrap">
				<div class="popUp_content feedBack">
					<img src="/f/close.svg" class="closePopUp" alt="">
					<div class="popUp_wrapFrom">
						<h2>cookie</h1>						
						<div class="btnWrap">
							<div class="borderBTN closePopUp_link"><span>Хорошо</span></div>
						</div>
					</div>
				</div>
				<div class="popUp_shade"></div>
			</div>
		</div>
		
		<!-- popup -->
		<div class="popUp" id="specialoffer">
			<div class="popUp_wrap">
				<div class="popUp_content feedBack">
					<img src="/f/close.svg" class="closePopUp" alt="">
					<div class="popUp_wrapFrom">
						<h2>Что то</h1>						
						<p>Текст</p>
						<div class="btnWrap">
							<div class="borderBTN closePopUp_link"><span>Хорошо</span></div>
						</div>
					</div>
				</div>
				<div class="popUp_shade"></div>
			</div>
		</div>

	</div>
	<!-- end content -->
	
	
	<div class="feedback">
		<div class="form flex flex-align_center">
			<div>
				<div class="close">
					<div class="button">
						<i></i>
						<i></i>
						<i></i>
					</div>
				</div>
				<div class="title">
					<h2>Оставить заявку</h2>
					<p>Отправьте вопрос и в ближайшее рабочее<br>время с вами свяжется менеджер</p>
					<div class="pattern"></div>
				</div>
				<form action="" method="get" name="form-2">
					<div class="param pasteParams__form-title"></div>
					<input type="hidden" name="title" class="formInput pasteParams__form-title" placeholder="">
					<input type="hidden" name="utm" class="formInput pasteParams__form-utm" placeholder="">

					<input type="text" name="name" class="formInput fillInput" placeholder="Ваше имя">
					<input type="text" name="tell" class="formInput fillInput tellmask" placeholder="Телефон">
					<button class="feedback btn btn-success borderBTN" onclick="yaCounter50791597.reachGoal ('sendform'); return true"><span>Оставить заявку</span></button>
					<div class="offerta">
						Отправляя заявку Вы подтверждаете свое согласие с нашей<br>
						<a href="#terms-of-use" data-link="#terms-of-use" class="openPopUp">политикой обработки персональных данных</a>
					</div>
				</form>
			</div>
		</div>
		<div class="shade"></div>
	</div>


</div>
<!-- end all	 -->

<div id="null_get"></div><div id="null_paste"></div>

<div class="policy-accept">
	<div class="flex --align-cennter --just-space">
		<div class="policy-accept__desc p --m type-editor">
			Используя сайт 3owls.ru, вы соглашаетесь с <a href="#terms-of-use" data-link="#terms-of-use" class="p --s openPopUp">использованием файлов cookie</a> и сервисов сбора технических данных посетителей (IP-адресов, местоположения и др.) для обеспечения работоспособности и улучшения качества обслуживания.
		</div>
		<div class="policy-accept__btn-wrap flex">
			<div class="policy-accept__btn fillBTN --js"><span>Принять</span></div>
		</div>
	</div>
</div>

<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/css/all.min.css">
<!-- <link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/css/ui.css">
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/css/preload.css">
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/libs/swiper/swiper.min.css" />
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/css/style.css">
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/css/style-1660.css">
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/css/style-1440.css">
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/css/style-1359.css">
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/css/style-1199.css">
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/css/style-991.css">
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/css/mob.css">
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/css/fonts.css">
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/libs/animate/animate.css">
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/libs/animate/new_animate.css">
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/libs/fullpage/fullpage.css" />
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/libs/fullpage/fullpagetwo.css" />
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/libs/fancybox/jquery.fancybox.min.css" />
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/libs/owl/owl.carousel.css">
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/libs/feedback/css/jquery.arcticmodal.css">
<link rel="preload" as="style" onload="this.rel='stylesheet'" type="text/css" href="/libs/feedback/css/jquery.jgrowl.css">	 -->	

<!-- <script type="text/javascript" src="/js/all.min.js"></script> -->
<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/lazyload.js"></script>	 
<script type="text/javascript" src="/libs/pjax/jquery.pjax.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/libs/fullpage/fullpage.js"></script>
<script type="text/javascript" src="/libs/fullpage/fullpagetwo.js"></script>
<script type="text/javascript" src="/libs/owl/owl.carousel.js"></script>
<script type="text/javascript" src="/libs/swiper/swiper.min.js"></script>
<script type="text/javascript" src="/libs/fancybox/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="/libs/inputmask/jquery.inputmask.bundle.js"></script>
<script type="text/javascript" src="/libs/spincrement/jquery.spincrement.js"></script>
<script type="text/javascript" src="/libs/jquery.cookie/jquery.cookie.js"></script>
<script type="text/javascript" src="/libs/feedback/js/feedback.js"></script>
<script type="text/javascript" src="/libs/feedback/js/jquery.arcticmodal.js"></script>
<script type="text/javascript" src="/libs/feedback/js/jquery.jgrowl.js"></script>
 

<?if($INDEX == true) :?>
	<!-- main page fix -->
	<script>
		var data__anchor = '';
		$(window).load(function(){
			fullpage_api.setAutoScrolling(false);
		});	
		$(document).ready(function(){
			$('body').on("click", ".pjax__project-link", function(){
				// activeSlide = window.location.hash.replace("#","");
				// data__anchor = $(this).parents('.section').data('anchor');
				 
				// alert(data__anchor);
		    });

		    $('body').on("click", ".menu .shade, .nav .back", function(){
		    	$('html').removeClass('fp-enabled');
		    	 
		  		// fullpage_api.setAutoScrolling(false);
				// console.log('go ' + activeSlide);
				// window.location.hash = activeSlide;
				fullpage_api.moveTo("solutions");
				// fullpage_apitwo.setAutoScrolling(false);
				fullpage_apitwo.destroy();
		    });			 
		});			
	</script>
<?endif;?>
<script>
	$(window).load(function(){
		$('#preloader').hide();
		// $('#MAIN').css({
		// 	'opacity': '1',
		// });
	});
</script>


<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(50791597, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript></noscript>
<!-- /Yandex.Metrika counter -->


</body>
</html>