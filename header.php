<!DOCTYPE html>
<html>
<head>
	<title><?=$META__TITLE?></title>
	<meta charset="utf-8">
	<meta name="description" content="<?=$META__DESC?>"/>
	<meta name="keywords" content="<?=$META__KEYS?>"/>	
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<meta name="google-site-verification" content="PZobTzKiuaLMG_15cWzU3D3Yhc1EWr-uwxUQbmDDuB8" />
	<meta name="yandex-verification" content="92f1e1cd1fc5bdc0" />
	<meta name='wmail-verification' content='6dddc4880a98c82af3f8b75fa5e85d21' />
	<!-- template -->
	<link rel="shortcut icon" href="/f/favicon.ico" type="image/x-icon" />
	<style>
		.menu .fullMenu {opacity: 0;}
		.pjax .wraper {transform: translateX(101vw);}
		#MAIN > .feedback .form {transform: translateX(-100%);}
		@media screen and (max-width:991px){
			.nav .logo{left:0.75rem;top:0.875rem;text-decoration:none;position:relative;z-index:2;transition:all ease 0.24s;width:8.625rem;transform:scale(0.7);height:3.3125rem;transition:all cubic-bezier(0.9,-0.01,0.53,0.98)0.54s;will-change:transition,opacity,left !important}
			.nav .logo .full{width:5.8125rem;height:1.375rem;background-repeat:no-repeat;background-image:url(../f/full.svg);margin-top:0.375rem;margin-left:0.25rem;transition:all ease 0.48s;will-change:transition,opacity,width;transition-delay:320ms;position:absolute;left:2.0625rem;will-change:transition,left,width,opacity,position}
			.nav .back {width: 5rem;transition-delay: 180ms;height: 5rem;}
		}
	</style>

</head>

<body class="<?if($PROJECT == true){?>open__fix<?}else{?><?}?> <?if($INDEX == true){?>page-index<?}?>">
	
	<div id="preloader" class="flex">
		<style>
			div#preloader {position: fixed;z-index: 10000;width: 100%;height: 100%;background-color: white;display: flex;justify-content: center;align-items: center;}
		</style>
		<svg viewBox="0 0 32 52" fill="none" xmlns="http://www.w3.org/2000/svg" style="opacity: 0.08;width: 4rem;">
		<path fill-rule="evenodd" clip-rule="evenodd" d="M0.000187709 0L0 7.16746V14.0248V14.3249V18.9463V19.9135V35.7596V51.94C3.61402 51.94 7.04284 51.1439 10.1222 49.7174C7.49585 48.5323 5.42366 47.002 3.90566 45.1261L8.30556 39.4548C9.65331 40.8477 11.3181 41.9619 13.3001 42.7977C15.282 43.5938 17.2639 43.9918 19.2459 43.9918C21.6242 43.9918 23.4674 43.534 24.7755 42.6188C26.0835 41.6636 26.7376 40.4298 26.7376 38.9176C26.7376 37.3254 26.1034 36.1712 24.835 35.4551C23.5665 34.6987 21.5449 34.3208 18.7702 34.3208C16.273 34.3208 14.628 34.3605 13.8352 34.4401V26.7987C14.3109 26.8387 15.9558 26.8586 18.7702 26.8586C23.6458 26.8586 26.0835 25.4456 26.0835 22.6198C26.0835 21.1076 25.3899 19.9534 24.0025 19.1574C22.6548 18.3218 20.8909 17.9039 18.7108 17.9039C14.8658 17.9039 11.5163 19.277 8.66233 22.023L4.44076 16.7098C8.16684 12.4912 13.2208 10.3819 19.6026 10.3819C23.4266 10.3819 26.5768 11.0448 29.0534 12.3706L31.5511 5.06971L25.0887 6.28795C22.4413 4.01481 18.6837 2.59529 14.5159 2.59529C11.2055 2.59529 8.15389 3.49088 5.7118 4.99835L0.000187709 0Z" fill="black"/>
		</svg>
	</div>
	<!-- all -->
	<div id="MAIN"  >
		
		<div class="nav <?if($PROJECT == true){?>open<?}else{?> fade<?}?>">
			<a href="/" class="logo flex flex-align_center">
				<div class="mini"></div>
				<div class="full"></div>
				<div class="desc">Creative, marketing, IT & advertising agency</div>
			</a>
			<?if($PROJECT == true){?>
				<a href="<?=$PROJECT_LINK?>" class="back"><i></i><div></div></a>
			<?}else{?>
				<? if ($_SERVER[REQUEST_URI]=="/") { ?>
					<a href="/" class="back pj pjax__back-link"><i></i><div></div></a>
				<? }?>
				<? if ($_SERVER[REQUEST_URI]=="/case/") { ?>
					<a href="/case/<?=$PROJECT_LINK?>" class="back pj pjax__back-link"><i></i><div></div></a>
				<? }?>
				<? if ($_SERVER[REQUEST_URI]=="/services/") { ?>
					<a href="/#case-2" class="back pj pjax__back-link"><i></i><div></div></a>
				<? }?>
				<? if ($_SERVER[REQUEST_URI]=="/contacts/") { ?>
					<a href="/" class="back pj pjax__back-link"><i></i><div></div></a>
				<? }?>
			<?}?>
			<div class="mob__place"></div>
		</div>
		
		<div class="menu <?if($PROJECT == true){?><?}else{?>fade<?}?>">
			<div class="language flex">
				<a class="language__link is-active">RU</a>
				<a href="/en<?=$_SERVER['REQUEST_URI']?>" class="language__link">EN</a>
			</div>
			<div class="button">
				<div class="flex flex-align_center">
					<div class="label">Меню</div>
					<div>
						<i></i>
						<i></i>
						<i></i>
					</div>
				</div>
			</div>
			<div class="fullMenu flex flex-align_center">
				<a href="/" class="logo flex flex-align_center">
					<div class="mini"></div>
					<div class="full"></div>
					<div class="desc">Creative, marketing, IT & advertising agency</div>
				</a>
				<div class="centerWrap flex">
					<ul>
						<li><a href="/">Главная</a></li>
						<li><a href="/contacts/">Контакты</a></li>
						<li>
							<a href="/#solutions">Решения</a>
							<ul>
								<li><a href="/services/web-development/onlinecalc/">Онлайн калькуляторы</a></li>
								<li><a href="/services/web-development/landing-page/">Landing-Page</a></li>
								<li><a href="/services/web-development/webar/">WebAR</a></li>
								<li><a href="/services/web-development/medical/">Медицинские сайты</a></li>
							</ul>
						</li>	
					</ul>
					<ul>
						<li>
							<a href="/case/">Кейсы</a>
							<ul>
							    <li><a href="/case/procosmetik/#screen-1">ProCosmetik</a></li>
							    <li><a href="/case/straus/#screen-1">Страус-Хаус</a></li>
							    <li><a href="/case/cleansman/#screen-1">CleansMan</a></li>
								<li><a href="/case/cutgo/#screen-1">Cut&Go</a></li>
								<!-- <li><a href="/case/parus/#screen-1">Парус</a></li> -->
								<li><a href="/case/mossles/#screen-1">Мослесгрупп</a></li>
								<li><a href="/case/onmaster/#screen-1">ОнМастер</a></li>
								<li style="opacity: .48;"><a href="/case/litiy/#screen-1">Литий</a></li>
								<li style="opacity: .32;"><a href="/case/bobrovo/#screen-1">Боброво</a></li>
								<li style="opacity: .24;"><a href="/case/mascotteconsulting/#screen-1">Mascotte Consulting</a></li>
								<!-- <li><a href="/case/ano/#screen-1">Медицинское образование</a></li> -->
								<!-- <li><a href="/case/juliya/#screen-1">Юлия Пахотина</a></li> -->
							</ul>
						</li>
					</ul>
					<ul class="--services">
						<li class="flex">
							<a href="/services/">Услуги</a>
							<ul>

								<li><a href="/services/#analytics">Аналитика</a></li>
								<li><a href="/services/#marketing">Маркетинг</a></li>
								<li>
									<a href="/services/#design">Дизайн</a>
									<ul>
										<li><a href="/services/web-development/webdesign/">Дизайн сайтов</a></li>
										<li><a href="/services/brandbook/">Брендбук</a></li>
									</ul>
								</li>
								<li>
									<a href="/services/web-development/">Разработка сайтов</a>
									<ul>
									    <li><a href="/services/web-development/landing-page/">Посадочные страницы</a></li>
									    <li><a href="/services/web-development/onlinecalc/">Одностраничные магазины</a></li>
										<li><a href="/services/web-development/verstka/">Верстка сайтов</a></li>
									</ul>
								</li>
							</ul>
							<ul>
								<li><a href="/services/smm/">SMM</a></li>
								<li><a href="/services/#sales">Продажи</a></li>
								<li>
									<a href="/services/#traffic-management">Трафик-менеджмент</a>
									<ul>
										<li><a href="/services/context-advertising/">Контекстная реклама</a></li>
									</ul>
								</li>
								<li><a href="/services/#crm">CRM-маркетинг</a></li>
								<li><a href="/services/#legal">Юридические услуги</a></li>
							</ul>
						</li>					
					</ul>
				</div>
			</div>
			<div class="shade"></div>
		</div>
		
		<div class="scrollDown"><i></i></div>
		
		<div class="pjax">
			<div class="wraper">
				<div id="pjax__project-content" <?if($COLOR){?> style="background-color: <?=$COLOR?>;" <?}?> ></div>
			</div>
			<div class="shade"></div>
		</div>

 		<!-- content -->
		<div id="CONTENT">