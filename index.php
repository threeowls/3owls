<?php 
    $META__TITLE = "3 owls - креативное, маркетинговое и рекламное агентство";
    $META__DESC = "Креативное, маркетинговое и рекламное агентство, предлагает своим клиентам разработку и дизайн digital и полиграфичкеской продукции, аналитику, продвижение бизнеса и его маркетинговое и информационное сопровождение.";
    $META__KEYS = "3 совы, трое сов, аналитика, креатив, маркетинг, реклама, разработка";
    $INDEX = true;
    require($_SERVER['DOCUMENT_ROOT'].'/header.php');
?>
 
<!-- <script type="text/javascript" src="/js/full__one.js"></script> -->

<div id="fullpage_catalog" class="page__index">
  
    <div class="section" data-anchor="main">
        <div class="mainSlide mainPage">
            <div class="wrap">
                <h1 class="type-js">Умные решения <br>для Вашего бизнеса</h1>
                <p>Мы - современное маркетинговое и рекламное агентство. Наши компетенции - вывод на рынок и продвижение Ваших товаров и услуг, увеличение продаж и улучшение Вашей существующей коммерческой, маркетинговой и рекламной деятельности.</p>
                <div class="pattern"></div>
            </div>
        </div>
        <div id="particles-js"></div>
        <script type="text/javascript" src="/libs/particles/particles.min.js"></script>
		<script type="text/javascript" src="/libs/particles/app.js"></script>
    </div>

	<div class="section fp-auto-height" data-anchor="services">
        <div class="mainSlide">
			<div class="tiles whatdoo">
				<h2 class="title">Что умеем?</h2>
				<div class="items flex flex-just_space">
					<!-- data:image/gif;base64,R0lGODlhCgAIAIABAN3d3f///yH5BAEAAAEALAAAAAAKAAgAAAINjAOnyJv2oJOrVXrzKQA7 -->
					<div class="item big lazyload" data-src="/f/whatdo__bg-1.png">
						<div class="wrap">
							<a href="/services/#analytics" class="title pjax__project-link">Аналитика</a>
							<div class="desc">Сделаем из данных инструмент для принятия решений</div>
							<div class="moreLink flex flex-just_center">
								<a href="/services/#analytics" class="pjax__project-link">Подробнее<img src="/f/more__link.svg" alt=""></a>
							</div>
						</div>
						<a href="/services/#analytics" class="bg__link pjax__project-link"></a>
					</div>
					<div class="item big lazyload" data-src="/f/whatdo__bg-2.png">
						<div class="wrap">
							<a href="/services/#webdev" class="title pjax__project-link">Сайты</a>
							<div class="desc">Web-разработка и интеграция сайтов в Ваши системы.</div>
							<div class="moreLink flex flex-just_center">
								<a href="/services/#webdev" class="pjax__project-link">Подробнее<img src="/f/more__link.svg" alt=""></a>
							</div>
						</div>
						<a href="/services/#webdev" class="bg__link pjax__project-link"></a>
					</div>
					<div class="item big lazyload" data-src="/f/whatdo__bg-3.png" style="background-size: 320px;" >
						<div class="wrap">
							<a href="/services/#marketing" class="title pjax__project-link">Маркетинг</a>
							<div class="desc">Упакуем, оформим и покажем услугу или товар Вашим будущим клиентам</div>
							<div class="moreLink flex flex-just_center">
								<a href="/services/#marketing" class="pjax__project-link">Подробнее<img src="/f/more__link.svg" alt=""></a>
							</div>
						</div>
						<a href="/services/#marketing" class="bg__link pjax__project-link"></a>
					</div>
					<div class="item big lazyload" data-src="/f/webdesign-min.png">
						<div class="wrap">
							<a href="/services/#design" class="title pjax__project-link">Дизайн</a>
							<div class="desc">Создадим функциональную красоту</div>
							<div class="moreLink flex flex-just_center">
								<a href="/services/#design" class="pjax__project-link">Подробнее<img src="/f/more__link.svg" alt=""></a>
							</div>
						</div>
						<a href="/services/#design" class="bg__link pjax__project-link"></a>
					</div>
					<div class="item big lazyload" data-src="/f/whatdo__bg-8.png">
						<div class="wrap">
							<a href="/services/#traffic-management" class="title pjax__project-link">Трафик-менеджмент</a>
							<div class="desc">Эффективно привлекаем будущих клиентов в Ваш бизнес</div>
							<div class="moreLink flex flex-just_center">
								<a href="/services/#traffic-management" class="pjax__project-link">Подробнее<img src="/f/more__link.svg" alt=""></a>
							</div>
						</div>
						<a href="/services/#traffic-management" class="bg__link pjax__project-link"></a>
					</div>
					<div class="item big lazyload" data-src="/f/whatdo__bg-5.png">
						<div class="wrap">
							<a href="/services/#sales" class="title pjax__project-link">Продажи</a>
							<div class="desc">Помогаем сделать из контакта с клиентом выручку</div>
							<div class="moreLink flex flex-just_center">
								<a href="/services/#sales" class="pjax__project-link">Подробнее<img src="/f/more__link.svg" alt=""></a>
							</div>
						</div>
						<a href="/services/#sales" class="bg__link pjax__project-link"></a>
					</div>
					<div class="item big lazyload" data-src="/f/smm-min.png" style=";background-size: 320px;">
						<div class="wrap">
							<a href="/services/smm/" class="title pjax__project-link">SMM</a>
							<div class="desc">Общайтесь со своей аудиторией напрямую</div>
							<div class="moreLink flex flex-just_center">
								<a href="/services/smm/" class="pjax__project-link">Подробнее<img src="/f/more__link.svg" alt=""></a>
							</div>
						</div>
						<a href="/services/smm/" class="bg__link pjax__project-link"></a>
					</div>
					<div class="item big lazyload" data-src="/f/whatdo__bg-7.png">
						<div class="wrap">
							<a href="/services/#legal" class="title pjax__project-link">Юридические услуги</a>
							<div class="desc">На тот самый всякий случай</div>
							<div class="moreLink flex flex-just_center">
								<a href="/services/#legal" class="pjax__project-link">Подробнее<img src="/f/more__link.svg" alt=""></a>
							</div>
						</div>
						<a href="/services/#legal" class="bg__link pjax__project-link"></a>
					</div>
				</div>
			</div>
        </div>
    </div>

    <div class="section" data-anchor="solutions">
        <div class="mainSlide">
			<div class="tiles solutuins">
				<h2 class="title">Решения для бизнеса</h2>
				<div class="items">
					<div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<div class="item lazyload" data-src="/f/reshenie__bg-1.png">
									<div class="wrap">
										<a href="/services//web-development/landing-page/#solutions" class="title pjax__project-link">Посадочные страницы</a>
										<div class="desc">Компактно. Четко. По делу.</div>
										<div class="moreLink flex flex-just_center">
											<a href="/services//web-development/landing-page/#solutions" class="pjax__project-link">Подробнее<img src="/f/more__link.svg" alt=""></a>
										</div>
									</div>
									<a href="/services//web-development/landing-page/#solutions" class="bg__link pjax__project-link"></a>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item lazyload" data-src="/f/reshenie__bg-2.png">
									<div class="wrap">
										<a href="/services/web-development/webar/#solutions" class="title pjax__project-link">Дополненная реальность</a>
										<div class="desc">Wow-эффект в вашем телефоне.</div>
										<div class="moreLink flex flex-just_center">
											<a href="/services/web-development/webar/#solutions" class="pjax__project-link">Подробнее<img src="/f/more__link.svg" alt=""></a>
										</div>
									</div>
									<a href="/services/web-development/webar/#solutions" class="bg__link pjax__project-link"></a>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item lazyload" data-src="/f/reshenie__bg-3.png">
									<div class="wrap">
										<a href="/services/web-development/onlinecalc/" class="title pjax__project-link">Одностраничные магазины</a>
										<div class="desc">Страница, которая работает как менеджер по продажам. А может и лучше.</div>
										<div class="moreLink flex flex-just_center">
											<a href="/services/web-development/onlinecalc/" class="pjax__project-link">Подробнее<img src="/f/more__link.svg" alt=""></a>
										</div>
									</div>
									<a href="/services/web-development/onlinecalc/" class="bg__link pjax__project-link"></a>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item lazyload" data-src="/f/medical_bg.png">
									<div class="wrap">
										<a href="/services/web-development/medical/" class="title pjax__project-link">Медицина</a>
										<div class="desc">Сайт для организаций <br>медицинской отрасли</div>
										<div class="moreLink flex flex-just_center">
											<a href="/services/web-development/medical/" class="pjax__project-link">Подробнее<img src="/f/more__link.svg" alt=""></a>
										</div>
									</div>
									<a href="/services/web-development/medical/" class="bg__link pjax__project-link"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="carusel__arr swiper-button-prev"></div>
					<div class="carusel__arr swiper-button-next"></div>
				</div>
			</div>
        </div>
    </div>
 	
 	<div class="section sect__wrap-no" data-anchor="cases">
 		<div class="project__wrap">
	    	<div class="wrap colls flex flex-just_space">
	    		<div class="coll flex flex-align_center">
	    			<div class="coll__wrap">
	    				<h2 style="width: 610px;">Наши кейсы</h2>
	    				<p>Почитайте. В наших историях 3 части опыта, <br>2 части труда и 1 часть юмора.</p>
	    				<br>
	    				<div class="pattern"></div>
	    				<div class="btnWrap">
	    					<a href="/case/" class="borderBTN"><span>Почитать кейсы</span></a>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="coll bg mobile__hide lazyload" data-src="/case/procosmetik/img/promo.png" style="background-position: right bottom;background-size: auto calc(100% - 7rem)">
	    		</div>
	    	</div>
    	</div>
    </div>

    <?php require($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'); ?>

</div>
<?php require($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>
