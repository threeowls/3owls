<?php 
	$META__TITLE = "Каталог услуг компании 3owls";
	$META__DESC = "Здесь Вы можете посмотреть список услуг нашего агентства, и выбрать, над чем мы можем поработать. Креатив, дизайн, маркетинг, аналитика, it-разработка и продвижение продуктов на рынке.";
	$META__KEYS = "Услуги маркетингового агентства";
	require($_SERVER['DOCUMENT_ROOT'].'/header.php');
?>

<!-- <script type="text/javascript" src="/js/fulltwo__one.js"></script> -->
<div id="pjax__project-load">
	<div id="fullpage_project" class="noPadding service_mainPage">

		<div class="section service" style="background-image: url(/f/service__bg-1.jpg)" data-anchor="analytics">
			<div class="mainSlide fadeContent">
		        <div class="wrap flex flex-align_end">
		        	<div>
						<div class="sect">Аналитика</div>
			            <h2>Сделаем из данных инструмент <br>для принятия решений</h2>
			            <div class="desc">
			            	<p>Определим цели анализа, структуру необходимых данных и предложим систему автоматизации их сбора и обработки. Покажем точки роста Вашей компании и узкие места в работе.</p>
			            </div>
			            <div class="pattern"></div>
			            <div class="services_secrions">
			            	<div class="line"></div>
			            	<div class="items flex flex-just_center">
			            		
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Аналитика <br>ассортимента</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Сквозная мультиканальная <br>аналитика</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Анализ <br>конкурентов</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>CRM <br>аналитика</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>

			            	</div>
			            </div>
		            </div>
		        </div>
	        </div>
	    </div>

	    <div class="section service" style="background-image: url(/f/whatdo__bg-3.jpg)" data-anchor="marketing">
			<div class="mainSlide fadeContent">
		        <div class="wrap flex flex-align_end">
		        	<div>
						<div class="sect">Маркетинг</div>
			            <h2>Упакуем, оформим и покажем услугу или товар Вашему будущему клиенту</h2>
			            <div class="desc">
			            	<p>Разработаем бренд и его рыночное позиционирование, составим план маркетинга и его бюджет, план рекламных кампаний и стратегию коммуникаций.</p>
			            </div>
			            <div class="pattern"></div>
			            <div class="services_secrions">
			            	<div class="line"></div>
			            	<div class="items flex flex-just_center">
			            		
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Брендирование <br>и позиционирование</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Планирование <br>маркетинга</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Рекламные <br>кампании</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Разработка <br>УТП</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            	</div>
			            </div>
		            </div>
		        </div>
	        </div>
	    </div>

	    <div class="section service" style="background-image: url(/f/whatdo__bg-4.jpg)" data-anchor="design">
			<div class="mainSlide fadeContent">
		        <div class="wrap flex flex-align_end">
		        	<div>
						<div class="sect">Дизайн</div>
			            <h2>Создадим функциональную красоту</h2>
			            <div class="desc">
			            	<p>Разработаем фирменный стиль и дизайн Вашей рекламной продукции.<br><b>Digital и Web-решения</b>: от банеров до сайтов любой сложности, дополненная реальность, 3d-моделирование и мобильные приложения.<br><b>Полиграфия</b>: от визиток до сложных каталогов.</p>
			            </div>
			            <div class="pattern"></div>
			            <div class="services_secrions">
			            	<div class="line"></div>
			            	<div class="items flex flex-just_center">
			            		
			            		<div class="item activated">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="/services/web-development/webdesign/" class="link"><span>Дизайн <br>сайтов </span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="/services/web-development/webdesign/"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Разработка <br>логотипов</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item activated">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X" style="width: 240px;"></div>
			            			</div>
			            			<a href="/services/brandbook/" class="link"><span>Печатная рекламная <br>продукция, бренд-бук</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="/services/brandbook/"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Дизайн мобильных <br>приложений</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>

			            	</div>
			            </div>
		            </div>
		        </div>
	        </div>
	    </div>

	    <div class="section service" style="background-image: url(/f/whatdo__bg-5.jpg)" data-anchor="webdev">
			<div class="mainSlide fadeContent">
		        <div class="wrap flex flex-align_end">
		        	<div>
						<div class="sect">Разработка сайтов</div>
			            <h2>Web-разработка и интеграция сайтов <br>в Ваши системы</h2>
			            <div class="desc">
			            	<p>Делаем как простые решения, так и сложные, интегрированные в Ваши системы автоматизации проекты. Одностраничные и многостраничные сайты, сайты-визитки, интернет-магазины, <br>корпоративные решения.</p>
			            </div>
			            <div class="pattern"></div>
			            <div class="services_secrions">
			            	<div class="flex flex-just_center">
			            		<a href="/services/web-development/" class="snoska mainLink"><span>Разработка сайтов</span></a>
			            	</div>
			            	<div class="line"></div>
			            	<div class="items flex flex-just_center">
			            		<div class="item activated">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="/services/web-development/verstka/" class="link"><span>Верстка <br>сайтов</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="/services/web-development/verstka/"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Сайты <br>визитки</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item activated">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="/services/web-development/landing-page/" class="link"><span>Продающие страницы <br>(Landing Pages)</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="/services/web-development/landing-page/"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X" style="    width: 216px;"></div>
			            			</div>
			            			<a href="#" class="link"><span>Корпоративные <br>сайты</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
				            	<div class="item activated">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="/services/web-development/onlinecalc/" class="link"><span>Одностраничные <br>интернет-магазины</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="/services/web-development/quiz/"></a></div>
			            		</div>
			            	</div>
			            </div>
		            </div>
		        </div>
	        </div>
	    </div>

		<div class="section d-service" data-anchor="smm">
	    	<div class="wrap colls flex flex-just_space">
	    		<div class="coll flex flex-align_center">
	    			<div class="coll__wrap fadeContent">
	    				<h1 class="boldTitle red">SMM - Social Media <br>Marketing</h1>
	    				<div class="h4 pre"><b>Управление присутствием бренда <br>в социальных сетях</b></div>
	    				<ul class="fixHeader">
							<li>Оформляем сообщество</li>
							<li>Разрабатываем контентную стратегию и контент-план </li>
							<li>Готовим контент и следим за его публикацией</li>
							<li>Вовлекаем посетителей во взаимодействия и продаём</li>
	    				</ul>
						<div class="btnWrap mrg_top">
							<a href="/services/smm/#section-2" class="borderBTN" data-title="SMM"><span>Подробнее</span></a>
						</div>
	    			</div>
	    		</div>
	    		<div class="coll flex flex-align_end flex-just_center mob__autoheight">
					<img src="/services/smm/img/header.png" alt="" class="fadeContent2 max580">
	    		</div>
	    	</div>
	    </div>

	   <!--  <div class="section service" style="background-image: url(/f/whatdo__bg-6.jpg)" data-anchor="smm">
			<div class="mainSlide fadeContent">
		        <div class="wrap flex flex-align_end">
		        	<div>
						<div class="sect">SMM</div>
			            <h2>Общайтесь со своей аудиторией напрямую</h2>
			            <div class="desc">
			            	<p>Организуем правильные коммуникации с Вашей аудиторией, разработаем контент-план и контент-стратегию, увеличим вовлеченность и продажи через социальные сети.</p>
			            </div>
			            <div class="pattern"></div>
			            <div class="services_secrions">
			            	<div class="line"></div>
			            	<div class="items flex flex-just_center">
			            		
			            		
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Разработка <br>контент-планов</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Продвижение сообществ <br>и аккаунтов</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X" style="    width: 176px;"></div>
			            			</div>
			            			<a href="#" class="link"><span>Оформление <br>сообществ</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Контент<br>менеджмент</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		
	

			            	</div>
			            </div>
		            </div>
		        </div>
	        </div>
	    </div> -->

		<div class="section service" data-anchor="sales">
			<div class="mainSlide fadeContent">
		        <div class="wrap flex flex-align_end">
		        	<div>
						<div class="sect">Продажи</div>
			            <h2>Помогаем сделать <br>из контакта с клиентом выручку</h2>
			            <div class="desc">
			            	<p>Оптимизируем работу отдела продаж, внедрим CRM-систему, скрипты для call-центра и проконсультируем по сложным продажам.</p>
			            </div>
			            <div class="pattern"></div>
			            <div class="services_secrions">
			            	<div class="line"></div>
			            	<div class="items flex flex-just_center">
			            		
			            		
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Разработка <br>скриптов</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Внедрение <br>CRM</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X" style="    width: 175px;"></div>
			            			</div>
			            			<a href="#" class="link"><span>Оптимизация <br>продаж</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Разработка <br>воронок</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		
	

			            	</div>
			            </div>
		            </div>
		        </div>
	        </div>
	    </div>

	    <div class="section service" style="background-image: url(/f/whatdo__bg-8.jpg)" data-anchor="traffic-management">
			<div class="mainSlide fadeContent">
		        <div class="wrap flex flex-align_end">
		        	<div>
						<div class="sect">Трафик-менеджмент</div>
			            <h2>Эффективно привлекаем будущих клиентов в Ваш бизнес</h2>
			            <div class="desc">
			            	<p>Организуем поток обращений в компанию.<br><b>Интернет-реклама</b>: контекстная, таргетированная, медийная и нативная реклама, работа через лидеров мнений.<br><b>Реклама в традиционных каналах</b>: наружная и печатная реклама, звуковая реклама в местах продаж, реклама на радио.</p>
			            </div>
			            <div class="pattern"></div>
			            <div class="services_secrions">
			            	<div class="line"></div>
			            	<div class="items flex flex-just_center">
			            		
								<div class="item activated">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="/services/advertising/" class="link"><span>Контекстная <br>реклама</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="/services/advertising/"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Таргетированная <br>реклама</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Offline <br>реклама</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Работа <br>с блогерами</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            	</div>
			            </div>
		            </div>
		        </div>
	        </div>
	    </div>

	    <div class="section service" data-anchor="crm">
			<div class="mainSlide fadeContent">
		        <div class="wrap flex flex-align_end">
		        	<div>
						<div class="sect">CRM-маркетинг</div>
			            <h2>Организуем работу с клиентской базой</h2>
                        <div class="desc">
			            	<p>Поможем оптимизировать и автоматизировать коммуникации Вашей компании с клиентами: удерживаем, развиваем, реактивируем.</p>
			            </div>
			            <div class="pattern"></div>
			            <div class="services_secrions">
			            	<div class="line"></div>
			            	<div class="items flex flex-just_center">
			            		
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Сегментируем <br>клиентскую базу</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Разрабатываем план <br>коммуникаций</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X" style="    width: 237px;"></div>
			            			</div>
			            			<a href="#" class="link"><span>Организуем <br>рассылки</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Формируем и отслеживаем <br>показатели эффективности</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
	

			            	</div>
			            </div>
		            </div>
		        </div>
	        </div>
	    </div>

	    <div class="section service" style="background-image: url(/f/whatdo__bg-10.jpg)" data-anchor="legal">
			<div class="mainSlide fadeContent">
		        <div class="wrap flex flex-align_end">
		        	<div>
						<div class="sect">Юридические услуги</div>
			            <h2>На тот самый всякий случай</h2>
			            <div class="desc">
			            	<p>Ответственные юристы с большим опытом работы и судебной практики.</p>
			            </div>
			            <div class="pattern"></div>
			            <div class="services_secrions">
			            	<div class="line"></div>
			            	<div class="items flex flex-just_center">
			            		
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Регистрируем <br>юридические лица, ИП</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Регистрируем <br>товарные знаки</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X" style="    width: 288px;"></div>
			            			</div>
			            			<a href="#" class="link"><span>Разрабатываем качественные <br>договора</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
			            		<div class="item">
			            			<div class="lines">
			            				<div class="Y"></div>
			            				<div class="X"></div>
			            			</div>
			            			<a href="#" class="link"><span>Ведём претензионную <br>работу и арбитраж</span></a>
			            			<div class="moreBTN flex flex-just_center"><a href="#"></a></div>
			            		</div>
	

			            	</div>
			            </div>
		            </div>
		        </div>
	        </div>
	    </div>
 		
 		<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'); ?>
	</div>
</div>
<?php require($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>